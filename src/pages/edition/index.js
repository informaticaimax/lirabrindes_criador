import React, { Component } from 'react';

import './styles.css';

import ProgressBar from '../../components/ProgressBar';
import Editor from '../../components/Editor';


import { FaArrowCircleRight } from 'react-icons/fa';

import api from '../../services/api';
import { storageGetCreation, storageGetHistoric, storageSetCreation } from '../../services/storage';

export default class Edition extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: parseInt(this.props.match.params.id),
            canvasPreview: null,

            imageToLayout: {},
            loading: true,
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            next: false,

            product: null,
            image: null,

            component: null,
            color: null,
            location: null,
            customization: null,
        };
    }

    async componentDidMount() {
        let creation = storageGetCreation(this.state.id);
        const historic = storageGetHistoric(this.state.id);

        if (!creation) {
            creation = historic;
        }
        const { product, image, imagePath, component, color, location, customization } = creation;
        const response = await api.get(`imagetolayout/${product.id}/${color}/`, {
            params: {
                component,
                location
            }
        });

        const imageToLayout = response.data.img;
        var img = new Image();
        img.src = imagePath;
        img.onload = () => {
            // configurações no src/styles.css
            const cssImgWidth = 0.9; // #preview > img, #preview-img
            const cssPreviewWidth = 0.35; // #preview
            const cssPreviewPadding = 50; // #preview

            let previewWidth = window.innerWidth * cssPreviewWidth;
            let previewAreaWidth = previewWidth - cssPreviewPadding;
            let imgResizedWidth = previewAreaWidth * cssImgWidth;

            if (img.width < imgResizedWidth) {
                imgResizedWidth = img.width * cssImgWidth;
                previewAreaWidth = img.width;
            }

            const ratio = imgResizedWidth / img.width;
            const mLeft = (previewAreaWidth - imgResizedWidth) / 2;

            const left = (imageToLayout.area_x * ratio) + mLeft;
            const top = imageToLayout.area_y * ratio;
            const width = imageToLayout.area_w * ratio;
            const height = imageToLayout.area_h * ratio;

            setTimeout(() => {
                let container = document.getElementsByClassName('canvas-preview-container');
                container = container[0];
                let canvasPreview = document.getElementById('canvas-preview');
                let mainCanvas = document.getElementById('canvas');
                if (canvasPreview) {
                    canvasPreview.style.borderRadius = `${imageToLayout.border_sup_esq}% ${imageToLayout.border_sup_dir}% ${imageToLayout.border_inf_dir}% ${imageToLayout.border_inf_esq}%`
                }
                if (mainCanvas) {
                    mainCanvas.style.borderRadius = `${imageToLayout.border_sup_esq}% ${imageToLayout.border_sup_dir}% ${imageToLayout.border_inf_dir}% ${imageToLayout.border_inf_esq}%`
                }

                if (container) {
                    container.style.position = `absolute`;
                    container.style.left = `${left}px`;
                    container.style.top = `${top}px`;
                    container.style.width = `${width}px`;
                    container.style.height = `${height}px`;
                }
            }, 0);


            this.setState({
                left,
                top,
                width,
                height,
                imageToLayout
            });
        }

        this.setState({
            loading: false,
            product, image, imagePath, component, color, location, customization,
        });
    }

    next = () => {
        this.setState({
            next: true
        });
    }
    back = () => {
        let previewJSON = localStorage.getItem('previewJSON');
        const creation = storageGetCreation(this.state.id);
        if (previewJSON) {
            creation.canvas = previewJSON;
        }
        creation.progress = 0;
        storageSetCreation(creation);
        this.props.history.push(`/init/${this.state.id}`);
    }


    render() {
        return (
            <div className="wrapper">
                <div id="preview">
                    {!this.state.loading &&
                        <>
                            <div id="preview-area">
                                <img src={this.state.imagePath} alt={this.state.product.name} id="preview-img" />
                                <canvas id="canvas-preview" />
                            </div>
                            <h2>Informações</h2>
                            <div dangerouslySetInnerHTML={{ __html: this.state.product.description }}></div>
                        </>
                    }
                </div>
                <div id="data">
                    <ProgressBar position={1} />
                    {this.state.width !== 0 && this.state.height !== 0 &&
                        <Editor idProduct={this.state.id} next={this.state.next} imageToLayout={this.state.imageToLayout} width={this.state.width} height={this.state.height} image={this.state.image} />
                    }
                    <div id="footer-actions">
                        <button className="btn btn-cancel" onClick={this.back}>Voltar</button>
                        <button className="btn btn-continue" onClick={this.next}>Continuar <FaArrowCircleRight /> </button>
                    </div>
                </div>
            </div>
        );
    }
}