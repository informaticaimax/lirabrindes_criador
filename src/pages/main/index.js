import React, { Component } from 'react';
// import ContentLoader from 'react-content-loader';

import './styles.css';

import ProgressBar from '../../components/ProgressBar';


import { FaArrowCircleRight } from 'react-icons/fa';
import api from '../../services/api';
import { storageSetCreation, storageGetCreation, storageGetHistoric } from '../../services/storage';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            creation: [],
            id: parseInt(this.props.match.params.id),

            loading: true,

            product: null,
            image: null,
            imagePath: null,

            components: [],
            colors: [],
            locations: [],
            customizations: [],

            component: null,
            color: null,
            location: null,
            customization: null
        };
    }

    async componentDidMount() {
        let creation = await storageGetCreation(this.state.id);
        let historic = await storageGetHistoric(this.state.id);
        if (historic) {
            if (!creation) {
                await storageSetCreation(historic);
                creation = historic;
            }

            if (creation.progress === 1) {
                this.props.history.push(`/edition/${this.state.id}`);
            } else if (creation.progress === 2) {
                this.props.history.push(`/final/${this.state.id}`);
            }
        }
        await api.get(`/product/${this.state.id}`)
            .then(res => {
                this.setState({
                    loading: false,
                    product: res.data.product,
                    imagePath: res.data.product.main_image_path,
                    components: res.data.components
                });
            });
        if (creation !== null && creation !== undefined) {
            await this.setComponent(creation.component);
            await this.setColor(creation.color);
            await this.setLocation(creation.location, creation.image, creation.imagePath)
            await this.setCustomization(creation.customization);
        }
    }

    setComponent = component => {
        if (component === this.state.component) return;
        this.setState({
            component,
            color: null,
            location: null,
            customization: null,

            colors: [],
            locations: [],
            customizations: []
        });

        api.get(`/product/colors/${this.state.id}/${component}`)
            .then(res => {
                const colors = res.data;
                this.setState({ colors });
            });
    }

    setColor = color => {
        if (color === this.state.color) return;

        this.setState({
            color,
            location: null,
            customization: null,

            locations: [],
            customizations: []
        });

        api.get(`/product/locations/${this.state.id}/${this.state.component}/${color}`)
            .then(res => {
                const locations = res.data;
                this.setState({ locations });
            });
    }

    setLocation = (location, image, imagePath) => {
        if (location === this.state.location) return;
        this.setState({
            location,
            image,
            imagePath,
            customization: null,

            customizations: []
        });

        api.get(`/product/customizations/${this.state.id}/${this.state.component}/${this.state.color}/${location}`)
            .then(res => {
                const customizations = res.data;
                this.setState({ customizations });
            });
    }

    setCustomization = customization => {
        if (customization === this.state.customization) return;

        this.setState({ customization });
    }

    next = () => {
        const validate = this.validate();

        if (!validate) return;
        const oldCreation = storageGetCreation(this.state.id);
        const creation = {
            progress: 1,
            id: this.state.id,
            product: this.state.product,
            image: this.state.image,
            imagePath: this.state.imagePath,
            component: this.state.component,
            color: this.state.color,
            location: this.state.location,
            customization: this.state.customization,
            preview: null,
            canvas: null,
            name: null,
            email: null,
            company: null,
            cnpj: null,
            phone: null,
            cellphone: null,
            upload: [],
            pantones: []
        };
        if (oldCreation) {
            if (
                oldCreation.component === creation.component &&
                oldCreation.color === creation.color &&
                oldCreation.location === creation.location &&
                oldCreation.customization === creation.customization
            ) {
                oldCreation.progress = 1;
                storageSetCreation(oldCreation);
            } else {
                storageSetCreation(creation);
            }
        } else {
            storageSetCreation(creation);
        }

        this.props.history.push(`/edition/${this.state.id}`);
    }

    validate = () => {
        if (this.state.component === null) {
            alert('Escolha o componente');
            return false;
        }

        if (this.state.color === null) {
            alert('Escolha a cor');
            return false;
        }

        if (this.state.location === null) {
            alert('Escolha a localização');
            return false;
        }

        if (this.state.customization === null) {
            alert('Escolha a customização');
            return false;
        }

        return true;
    }

    render() {
        return (
            <div className="wrapper">
                <div id="preview">
                    {!this.state.loading &&
                        <>
                            <img src={this.state.imagePath} alt={this.state.product.name} className="preview-img" />
                            <h2>Informações</h2>
                            <div dangerouslySetInnerHTML={{ __html: this.state.product.description }}></div>
                        </>
                    }
                </div>
                <div id="data">

                    {!this.state.loading &&
                        <>
                            <ProgressBar position={0} />
                            <h1>Selecione as características</h1>
                            <p>Selecione um componente, localização e técnica de impressão. No final da personalização poderá escolher outro componente.</p>

                            <fieldset>
                                <legend>Escolha o componente</legend>
                                {this.state.components.map((component, index) => (
                                    <button
                                        className={`component-option ${component.component === this.state.component ? 'active' : ''}`}
                                        key={index}
                                        onClick={() => this.setComponent(component.component)}
                                    >
                                        <img src={component.image_path} alt="Imagem" />
                                        <p>{component.component}</p>
                                    </button>

                                ))}

                            </fieldset>

                            <fieldset>
                                <legend>Escolha a cor</legend>
                                {this.state.component !== null &&
                                    <>
                                        {this.state.colors.map((color, index) => (
                                            <button
                                                key={index}
                                                className={`color-option ${color.id === this.state.color ? 'active' : ''}`}
                                                onClick={() => this.setColor(color.id)}
                                                title={color.name}
                                            >
                                                <img src={color.image_path} alt={color.name} />
                                            </button>
                                        ))}
                                    </>
                                }
                                {this.state.component === null &&
                                    <p>Aguardando a escolha do componente.</p>
                                }
                            </fieldset>

                            <fieldset>
                                <legend>Escolha a localização</legend>
                                {this.state.color !== null &&
                                    <>
                                        {this.state.locations.map((location, index) => (
                                            <button
                                                className={`location-option ${location.location === this.state.location ? 'active' : ''}`}
                                                key={index}
                                                onClick={() => this.setLocation(location.location, location.image, location.image_path)}
                                            >
                                                <img src={location.image_path} alt="Imagem" />
                                                <p>{location.location}</p>
                                            </button>
                                        ))}
                                    </>
                                }
                                {this.state.color === null &&
                                    <p>Aguardando a escolha da cor.</p>
                                }
                            </fieldset>

                            <fieldset>
                                <legend>Escolha a técnica de impressão</legend>
                                {this.state.location !== null &&
                                    <select
                                        value={this.state.customization}
                                        onChange={e => this.setCustomization(e.target.value)}
                                    >
                                        <option value={null}>Escolha uma opção...</option>
                                        {this.state.customizations.map((customization, index) => (
                                            <option key={index} value={customization.id}>{customization.name}</option>
                                        ))}
                                    </select>
                                }
                                {this.state.location === null &&
                                    <p>Aguardando a escolha da localização.</p>
                                }
                            </fieldset>

                            <div id="footer-actions">
                                <button className="btn btn-cancel">Cancelar</button>
                                <button className="btn btn-continue" onClick={() => this.next()}>Continuar <FaArrowCircleRight /> </button>
                            </div>
                        </>
                    }
                </div>
            </div>
        );
    }
}