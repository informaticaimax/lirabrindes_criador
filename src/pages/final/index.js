import React, { Component } from 'react';

import ProgressBar from '../../components/ProgressBar';
import api from '../../services/api';
import InputMask from 'react-input-mask';

import { FaArrowCircleRight } from 'react-icons/fa';
import { MdDelete } from 'react-icons/md';
import { storageGetCreation, storageSetCreation, storageRemoveCreation, storageRemoveHistoric, storageRemoveItem, storageGetHistoric } from '../../services/storage';

import './styles.css';
import load from '../../assets/images/load.gif';

export default class Final extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: parseInt(this.props.match.params.id),
            canvasPreview: null,

            loading: true,

            product: null,

            color: null,
            creation: [],

            pantoneList: [],

            pantone: false,
            name: '',
            email: '',
            company: '',
            cnpj: '',
            phone: '',
            cellphone: '',
            load: false
        };
    }
    async componentDidMount() {
        const creation = storageGetCreation(this.state.id);
        const { product, color, customization, name, email, company, cnpj, phone, cellphone } = creation;
        const responseCustomization = await api.get(`customization/${customization}`);
        const customizationObj = responseCustomization.data;

        const preview = `https://www.lirabrindes.com/uploads/creations/${creation.preview}`;
        this.setState({
            loading: false,
            pantone: customizationObj.pantone === "1",
            product, preview, color, customization, creation, name, email, company, cnpj, phone, cellphone
        });
    }

    finish = async () => {
        const validate = this.validate();
        if (validate === true) {
            try {
                this.setState({
                    load: true
                });
                await api.post(`finish/${this.state.id}/${this.state.creation.color}`, {
                    creation: this.state.creation,
                    name: this.state.name,
                    email: this.state.email,
                    company: this.state.company,
                    cnpj: this.state.cnpj,
                    phone: this.state.phone,
                    pantones: this.state.pantoneList,
                    cellphone: this.state.cellphone,
                });
                const creation = storageGetCreation(parseInt(this.state.id));
                const historic = storageGetHistoric(this.state.id);
                storageRemoveCreation(creation);
                storageRemoveHistoric(historic);
                storageRemoveItem('previewJSON');

                window.parent.postMessage({
                    'func': 'hideCreator',
                    'success': true
                }, "*");
            } catch (error) {
                this.setState({
                    load: false
                });
                console.log(error.response);
            }
        }
    }
    back = () => {
        const creation = this.state.creation;
        creation.progress = 1;
        storageSetCreation(creation);
        this.props.history.push(`/edition/${this.state.id}`);
    }
    validate = () => {
        if (this.state.name === null) {
            alert('Digite o nome');
            return false;
        }

        if (this.state.email === null) {
            alert('Digite o e-mail');
            return false;
        }

        if (this.state.company === null) {
            alert('Digite o nome da empresa');
            return false;
        }

        if (this.state.cnpj === null) {
            alert('Digite o CNPJ');
            return false;
        }
        if (this.state.phone === null) {
            alert('Digite o telefone comercial');
            return false;
        }
        if (this.state.cellphone === null) {
            alert('Digite o celular');
            return false;
        }

        return true;
    }
    handlePantone = (e) => {
        e.preventDefault();
        const pantoneList = this.state.pantoneList;

        pantoneList.push({
            name: ''
        });
        this.setState({
            pantoneList
        });
    }
    handlePantoneItem = (e, key) => {
        const pantoneList = this.state.pantoneList;
        pantoneList[key].name = e.target.value;
        this.setState({
            pantoneList
        });
    }
    removePantone = (key) => {
        const pantoneList = this.state.pantoneList;
        pantoneList.splice(key, 1);
        this.setState({
            pantoneList
        });
    }
    handleChange = e => {
        const creation = this.state.creation;
        creation[e.target.name] = e.target.value;
        this.setState({
            [e.target.name]: e.target.value,
            creation
        });
        storageSetCreation(creation);
    }
    render() {
        return (
            <div className="wrapper">
                <div id="preview">
                    {!this.state.loading &&
                        <>
                            <div id="preview-area">
                                <img src={this.state.preview} alt={this.state.product.name} id="preview-img" />
                                {/* <canvas id="canvas-preview" /> */}
                            </div>
                            <h2>Informações</h2>
                            <div dangerouslySetInnerHTML={{ __html: this.state.product.description }}></div>
                        </>
                    }
                </div>
                <div id="data">
                    {this.state.load &&
                        <div className="load">
                            <img src={load} alt="Carregando" />
                        </div>
                    }
                    {!this.state.load &&
                        <>
                            <ProgressBar position={2} />
                            <div className="row">
                                <div className="col">
                                    <p className="message">Preencha com os dados abaixo para receber sua amostra virtual no e-mail.</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-2">
                                    <fieldset>
                                        <legend>Nome</legend>
                                        <input
                                            name="name"
                                            value={this.state.name}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                                <div className="col-2">
                                    <fieldset>
                                        <legend>E-mail</legend>
                                        <input
                                            name="email"
                                            type="email"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-2">
                                    <fieldset>
                                        <legend>Empresa</legend>
                                        <input
                                            name="company"
                                            value={this.state.company}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                                <div className="col-2">
                                    <fieldset>
                                        <legend>CNPJ</legend>
                                        <InputMask
                                            name="cnpj"
                                            mask="99.999.999/9999-99"
                                            value={this.state.cnpj}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-2">
                                    <fieldset>
                                        <legend>Telefone Comercial</legend>
                                        <InputMask
                                            name="phone"
                                            mask="(99) 9999-9999"
                                            value={this.state.phone}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                                <div className="col-2">
                                    <fieldset>
                                        <legend>Telefone Celular</legend>
                                        <InputMask
                                            name="cellphone"
                                            mask="(99) 99999-9999"
                                            value={this.state.cellphone}
                                            onChange={this.handleChange}
                                        />
                                    </fieldset>
                                </div>
                            </div>

                            {this.state.pantone === true &&
                                <fieldset>
                                    <legend>Lista de Pantones</legend>
                                    {this.state.pantoneList.map((item, key) => (
                                        <div className="pantone-box" key={key}>
                                            <button type="button" className="delete-item" onClick={() => this.removePantone(key)}><MdDelete /></button>
                                            <input
                                                value={item.name}
                                                onChange={e => this.handlePantoneItem(e, key)}
                                            />
                                        </div>
                                    ))
                                    }
                                    <button type="button" className="add-pantone" onClick={this.handlePantone}>Adicionar Pantone</button>
                                </fieldset>
                            }
                            <div id="footer-actions">
                                <button className="btn btn-cancel" onClick={this.back}>Voltar</button>
                                <button className="btn btn-continue" onClick={this.finish}>Finalizar <FaArrowCircleRight /> </button>
                            </div>
                        </>
                    }
                </div>
            </div>
        );
    }
}
