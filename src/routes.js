import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from './components/Header';
import Main from './pages/main';
import Edition from './pages/edition';
import Final from './pages/final';
import Home from './pages/home';

const Routes = () => (
    <BrowserRouter>
        <Header />
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/init/:id" component={Main} />
            <Route exact path="/edition/:id" component={Edition} />
            <Route exact path="/final/:id" component={Final} />
        </Switch>
    </BrowserRouter>
)

export default Routes;