export const convertDateToBrazil = value => {
    let dArr = value.split("-");
    return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0];
}

export const convertDateToAmerican = value => {
    let dArr = value.split("/");
    return dArr[0]+ "-" +dArr[1]+ "-" +dArr[2];  
}