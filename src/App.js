import React from 'react';

import 'normalize.css';
import './styles.css';

import Routes from './routes';

const App = () => (
  <Routes />
);

export default App;
