import React, { useState, useEffect, useMemo } from 'react';

import { useHistory } from 'react-router-dom';

import './styles.css';

import { fabric } from "fabric";

import { TiArrowForward } from 'react-icons/ti';
import { TiArrowBack } from 'react-icons/ti';
import { BsFillImageFill } from 'react-icons/bs';
import { ImTextColor } from 'react-icons/im';
import { FaRegCopy, FaArrowCircleRight } from 'react-icons/fa';
import { BiTrash } from 'react-icons/bi';
import { FiCopy } from 'react-icons/fi';
import { BiBold } from 'react-icons/bi';
import { CgAlignBottom, CgAlignMiddle, CgAlignTop, CgAlignLeft, CgAlignCenter, CgAlignRight, CgFormatItalic } from 'react-icons/cg';
import { useDropzone } from 'react-dropzone';
import ReactModal from 'react-modal';
import { SketchPicker } from 'react-color';
import api from '../../services/api';
import { storageGetCreation, storageSetCreation } from '../../services/storage';
import 'fabric-history';

import load from '../../assets/images/load.gif';

ReactModal.setAppElement('#root');
const Editor = (props) => {

    const history = useHistory();

    // const [idProduct] = useState(props.idProduct);
    const [canvas, setCanvas] = useState(null);
    const [canvasPreview, setCanvasPreview] = useState(null);
    const [zoom, setZoom] = useState(0);
    const [width] = useState(props.width);
    const [height] = useState(props.height);
    const [imageColor, setImageColor] = useState('#000000');
    const [isEffect, setIsEffect] = useState(false);
    const [selectedCustomization, setSelectedCustomization] = useState(null);
    const [loading, setLoading] = useState(false);



    //Executa uma unica vez
    useEffect(() => {
        api.get(`/customization/${creation.customization}`)
            .then(res => {
                setSelectedCustomization(res.data);
                res.data.type_effect === 0 ? setIsEffect(true) : setIsEffect(false);
            });

        const percentSecurity = 0.75;
        const editorArea = document.getElementById('editor-area');
        const editorWidth = editorArea.offsetWidth * percentSecurity;
        const editorHeight = editorArea.offsetHeight * percentSecurity;
        let canvasWidth = width;
        let canvasHeight = height;

        if (canvasWidth < editorWidth && canvasHeight < editorHeight) {
            let ratio;
            if (canvasWidth > canvasHeight) {
                ratio = editorWidth / canvasWidth;
            } else {
                ratio = editorHeight / canvasHeight;
            }

            setZoom((1 / ratio));

            canvasWidth = canvasWidth * ratio;
            canvasHeight = canvasHeight * ratio;
        }
        const loadCanvas = (canvas) => {
            if (creation.canvas !== null) {
                canvas.loadFromJSON(JSON.parse(creation.canvas), canvas.renderAll.bind(canvas));
            }
        }

        const initCanvas = (id = 'canvas') => {
            const canvas = new fabric.Canvas(id, {
                height: canvasHeight,
                width: canvasWidth,
            });
            loadCanvas(canvas);
            setClone(true);

            return canvas;
        }

        setCanvas(initCanvas());
        const initCanvasPreview = (id = 'canvas-preview') => {
            const canvasPreview = new fabric.Canvas(id, {
                height,
                width,
                containerClass: 'canvas-preview-container'
            });
            return canvasPreview;
        }

        setCanvasPreview(initCanvasPreview());
        // document.addEventListener('keyup', (event) => {
        //     if (!event.ctrlKey) {
        //         return
        //     }
        //     if (canvas === null) {
        //         return;
        //     }
        //     console.log(canvas)
        //     if (event.ctrlKey && event.key === 'z') {
        //         console.log('chamou aqui');
        //         handleUndo();
        //     }
        //     if (event.key === 'Delete') {
        //         handleRemove();
        //     }
        //     if (event.ctrlKey && event.shiftKey && event.key === 'z') {
        //         handleRedo();
        //     }
        // });
    }, [height, props.idProduct, width]);

    const handleUserKeyPress = event => {
        const { keyCode, ctrlKey } = event;
        // if (canvas === null) {
        //     return;
        // }
        if (!ctrlKey && keyCode !== 46) {
            return
        }
        // Check pressed button is delete.
        if (keyCode === 46) {
            handleRemove();
        }
        // Check pressed button is Z - Ctrl+Z.
        if (keyCode === 90) {
            handleUndo();
        }
        // Check pressed button is Y - Ctrl+Y.
        if (keyCode === 89) {
            handleRedo();
        }
    };
    useMemo(() => handleUserKeyPress, [handleUserKeyPress])
    useEffect(() => {
        window.addEventListener('keyup', handleUserKeyPress);

        return () => {
            window.removeEventListener('keyup', handleUserKeyPress);
        };
    }, [handleUserKeyPress]);
    const [visibleTextToolbar, setVisibleTextToolbar] = useState('');
    const [rotate, setRotate] = useState(0);
    const [fontSize, setFontsize] = useState(24);
    // const [isBold, setIsBold] = useState(false);
    // const [isItalic, setIsItalic] = useState(false);
    const [creation, setCreation] = useState(storageGetCreation(props.idProduct));
    const [fontFamily, setFontFamily] = useState('Arial');
    const [clone, setClone] = useState(false);

    // Eventos do canvas
    useEffect(() => {
        const cloneCanvas = () => {
            const previewJSON = canvas.toJSON();
            localStorage.setItem('previewJSON', JSON.stringify(previewJSON));
            canvasPreview.loadFromJSON(previewJSON, canvasPreview.renderAll.bind(canvasPreview));
            canvasPreview.setZoom(zoom);
        }
        const handleObjSelected = () => {
            const obj = canvas._activeObject;
            const type = obj.get('type');
            setRotate(parseInt(obj.angle));

            if (type === 'textbox') {
                setFontsize(obj.fontSize);
                setFontFamily(obj.fontFamily)
                // setIsBold()
                // setIsItalic()
                setVisibleTextToolbar('active');
            } else {
                setVisibleTextToolbar('');
            }
        }

        const handleObjUnselected = () => {
            setVisibleTextToolbar('');
        }

        const handleRender = () => {
            const obj = canvas._activeObject;
            if (obj) {
                setRotate(parseInt(obj.angle, 10));
            }
            cloneCanvas();
        }
        if (clone === true) {
            cloneCanvas();
        }

        if (!canvas) return;
        canvas.on("selection:created", handleObjSelected);
        canvas.on("selection:updated", handleObjSelected);
        canvas.on("selection:cleared", handleObjUnselected);
        canvas.on("after:render", handleRender);

        return () => {
            canvas.off("selection:created");
            canvas.off("selection:cleared");
            canvas.off("after:render");
        }
    }, [
        canvas,
        canvasPreview,
        visibleTextToolbar,
        fontSize,
        fontFamily,
        zoom,
        clone
    ]);

    const [files, setFiles] = useState([]);
    // Eventos do dropzone no modal
    useEffect(() => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files]);

    // Avança para a proxima fase
    useEffect(() => {
        const generateImage = async () => {
            const canvasFile = canvasPreview.toDataURL({
                format: 'png',
                multiplier: (props.imageToLayout.area_w / width),
                quality: 1
            });

            const data = {
                canvasFile,
                image: props.image,
                imageToLayout: props.imageToLayout
            };

            try {
                const response = await api.post(`/generateimage`, data);
                creation.preview = response.data;
                creation.progress = 2;
                creation.canvas = localStorage.getItem('previewJSON');
                setCreation(creation);
                storageSetCreation(creation);
                history.push(`/final/${props.idProduct}`)
            } catch (err) {
                console.log(err);
            }
        }
        if (props.next === true) {
            generateImage();
        }
    }, [props.next, creation, canvas, canvasPreview, history, props.idProduct, props.image, props.imageToLayout, width]);
    const [modalIsOpen, setIsOpen] = useState(false);

    const handleUndo = () => canvas.undo();
    const handleRedo = () => canvas.redo();

    const handleRemove = () => canvas.remove(canvas._activeObject);

    const handleClone = () => {
        const objActive = canvas._activeObject
        const obj = fabric.util.object.clone(objActive);
        if (objActive) {
            obj.set("top", obj.top + 10);
            obj.set("left", obj.left + 10);
            canvas.add(obj);
        }
        canvas.renderAll();
    }

    const addText = () => {
        const text = new fabric.Textbox('Seu texto', {
            fontSize: 24,
            width: 120,
            fontFamily: 'Arial',
            top: width * 2,
            left: width * 2,
            originX: 'center',
            originY: 'center'
        });

        canvas.add(text);
        canvas.renderAll();
    }

    const handleOrderUp = () => {
        const obj = canvas._activeObject;
        if (obj) {
            obj.bringToFront();
        }
        canvas.discardActiveObject();
        canvas.renderAll();
    }
    const handleOrderDown = () => {
        const obj = canvas._activeObject;
        if (obj) {
            obj.sendToBack();
        }

        canvas.discardActiveObject();
        canvas.renderAll();
    }

    const handleFontSize = e => {
        const obj = canvas._activeObject;
        if (obj) {
            const type = obj.get('type');
            if (type === "textbox") {
                obj.fontSize = parseInt(e.target.value, 10);
                setFontsize(parseInt(e.target.value, 10));
                obj.set('scaleX', 1);
                obj.set('scaleY', 1);
            }
        }
        canvas.renderAll();
    }
    const handleBold = () => {
        const obj = canvas._activeObject;
        if (obj) {
            const type = obj.get('type');
            if (type === "textbox") {
                let fontWeight = obj.fontWeight === 'bold' ? 'normal' : 'bold';
                obj.fontWeight = fontWeight;
            }
        }
        canvas.renderAll();
    }
    const handleItalic = () => {
        const obj = canvas._activeObject;
        if (obj) {
            const type = obj.get('type');
            if (type === "textbox") {
                let fontStyle = obj.fontStyle === 'italic' ? 'normal' : 'italic';
                obj.fontStyle = fontStyle;
            }
        }
        canvas.renderAll();

    }

    const handleRotation = e => {
        let val = e.target.value !== '' ? e.target.value : 0;
        const obj = canvas._activeObject;
        if (obj) {
            obj.rotate(parseInt(val, 10));
            setRotate(parseInt(val, 10));
        }
        canvas.renderAll();
    }
    const handleFontFamily = e => {
        const obj = canvas._activeObject;
        if (obj) {
            const type = obj.get('type');
            if (type === "textbox") {
                obj.fontFamily = e.target.value;
                setFontFamily(e.target.value);
            }
        }
        canvas.renderAll();

    }

    const handleColor = e => {
        const obj = canvas._activeObject;
        if (obj) {
            const type = obj.get('type');
            if (type === "textbox") {
                obj.set("fill", e.target.value);
            }
        }
        canvas.renderAll();

    }

    const alignCenter = (obj,) => {
        const itemWidth = obj.getBoundingRect().width;
        obj.set({
            left: (0 - itemWidth / 2),
            originX: 'left'
        });
        obj.setCoords();
    }

    const alignLeft = (obj, groupWidth) => {
        const itemWidth = obj.getBoundingRect().width;
        obj.set({
            left: -groupWidth / 2 + itemWidth / 2
        });
        obj.setCoords();
    }
    const alignRight = (obj, groupWidth) => {
        const itemWidth = obj.getBoundingRect().width;
        obj.set({
            left: groupWidth / 2 - itemWidth / 2
        });
        obj.setCoords();
    }

    const alignBottom = (obj, groupHeight) => {
        const itemHeight = obj.getBoundingRect().height;
        obj.set({
            top: groupHeight / 2 - itemHeight / 2
        });
    }
    const alignMiddle = (obj) => {
        const itemHeight = obj.getBoundingRect().height;
        obj.set({
            top: (0 - itemHeight / 2),
        });
        obj.setCoords();
    }
    const alignTop = (obj, groupHeight) => {
        const itemHeight = obj.getBoundingRect().height;
        obj.set({
            top: -groupHeight / 2 + itemHeight / 2
        });
    }

    const handleAlign = (align) => {
        const activeObj = canvas._activeObject || canvas.getActiveObjects();
        if (activeObj.length === 0) {
            return;
        }
        const groupWidth = activeObj.getBoundingRect(true).width;
        const groupHeight = activeObj.getBoundingRect(true).height;


        switch (align) {
            case 'alignBottom':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignBottom(obj, groupHeight);
                    });
                } else {
                    alignBottom(activeObj, groupHeight);
                }
                break;
            case 'alignMiddle':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignMiddle(obj);
                    });
                } else {
                    activeObj.centerV();
                }
                break;
            case 'alignTop':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignTop(obj, groupHeight);
                    });
                } else {
                    alignTop(activeObj, groupHeight);
                }
                break;
            case 'alignLeft':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignLeft(obj, groupWidth);
                    });
                } else {
                    alignLeft(activeObj, groupWidth);
                }
                break;
            case 'alignCenter':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignCenter(obj);
                    });
                } else {
                    activeObj.centerH();
                }
                break;
            case 'alignRight':
                if (activeObj.get('type') === 'activeSelection') {
                    activeObj.getObjects().forEach(obj => {
                        alignRight(obj, groupWidth);
                    });
                } else {
                    activeObj.set({
                        left: canvas.getWidth() - activeObj.getBoundingRect().width
                    });
                }
                break;
            default:
                break;
        }
        canvas.renderAll();
    }

    const thumbsContainer = {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 312,
        height: 200,
        marginTop: 16
    };

    const thumb = {
        display: 'inline-flex',
    };

    const thumbInner = {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    };

    const img = {
        display: 'block',
        objectFit: 'contain',
        width: '100%',
        height: '100%'
    };

    const { getRootProps, getInputProps } = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setLoading(true);
            handleUpload(acceptedFiles);
        }

    });

    const handleUpload = async (files, color = null) => {
        const { customization } = creation;

        // setFiles(files.map(file => Object.assign(file, {
        //     preview: URL.createObjectURL(file)
        // })));

        files.forEach(file => {

            let data = new FormData();
            if (color === null) {
                color = imageColor;
            }

            data.append('file', file);
            data.append('customization', customization);
            data.append('color', color);
            setFiles([]);

            api.post(`/product/${props.idProduct}/image`, data)
                .then(async res => {
                    setLoading(false);

                    const blob = await fetch(res.data.path)
                        .then(res => { return res.blob() });

                    const f = new File([blob], res.data.name, { type: "image/png" });
                    setFiles([Object.assign(file, {
                        preview: URL.createObjectURL(f),
                        url: res.data.url
                    })]);

                })
                .catch(er => {
                    let { response } = er;
                    if (response.status === 401) {
                        if (response.data.success === false) {
                            alert(response.data.message);
                        }
                    }
                });

        });
    }

    const handleImageColor = ({ hex }) => setImageColor(hex);
    // setLoading(true);
    // await handleUpload(files, e.target.value);

    const handleCompleteColor = ({ hex }) => {
        setLoading(true);
        handleUpload(files, hex);
    }

    // const dataURLtoFile = (url, filename, mimeType) => {
    //     return (fetch(url)
    //         .then(function (res) { return res.arrayBuffer(); })
    //         .then(function (buf) { return new File([buf], filename, { type: mimeType }); })
    //     );
    // }

    const thumbs = files.map(file => (
        <div style={thumbInner} key={file.name}>
            <img
                src={file.preview}
                style={img}
                alt={file.name}
            />
        </div>
    ));

    const submitModal = () => {
        files.forEach(file => {
            new fabric.Image.fromURL(file.url, function (oImg) {
                oImg.scaleToWidth(canvas.getWidth() / 2);
                oImg.set("left", width * 2);
                oImg.set("right", width * 2);
                oImg.set('originX', 'center',);
                oImg.set('originY', 'center');
                canvas.add(oImg);
            });
            let urlArray = file.url.split('tmp/');
            creation.upload.push(urlArray[1]);
            setCreation(creation);
            storageSetCreation(creation);
        });
        setFiles([]);
        setIsOpen(false);
    }
    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)'
        }
    };

    return (
        <div id="editor">
            <ReactModal
                isOpen={modalIsOpen}
                onRequestClose={() => setIsOpen(false)}
                style={customStyles}
            >
                <h2>Realizar upload de imagem</h2>
                <div>Arraste o seu arquivo de imagem para a zona delimitada abaixo. A imagem terá de ser em alta resolução.</div>
                <aside className="image-preview"></aside >
                <div className="upload">
                    <div className="dropzone-box">
                        <section className="container">
                            <div {...getRootProps({ className: 'dropzone' })}>
                                <input {...getInputProps()} />
                                <p>Arraste Arquivos ou clique aqui</p>
                            </div>
                            <aside style={thumbsContainer}>
                                {loading &&
                                    <div className="img-load">
                                        <img src={load} alt="Carregando" />
                                    </div>
                                }
                                {thumbs}
                            </aside>
                        </section>
                    </div>
                    <div className="information">
                        <h4>Parâmetros</h4>
                        <p>Os formatos aceitos são JPG e PNG</p>
                        {/* <p>A resolução deve ser maior que 200dpi</p> */}
                        <p>Largura e Altura devem ser maior do que 300px</p>
                        {isEffect &&
                            <>
                                <br />
                                <p>
                                    <b>Este tipo de impressão só aceita uma cor.</b><br />
                                    Por favor, escolha a cor que deseja para a imagem.
                                </p>
                                <SketchPicker
                                    color={imageColor}
                                    onChange={handleImageColor}
                                    onChangeComplete={handleCompleteColor}
                                />
                                {/* <input type="color" className="input-color-img" onChange={handleImageColor} name="imageColor" value={imageColor} /> */}
                                {/* <label className="label-color-img" style={{ color: imageColor }}>{imageColor}</label> */}
                            </>
                        }

                    </div>
                </div>
                <div id="footer-actions">
                    <button className="btn btn-cancel" onClick={() => setIsOpen(false)}>Cancelar</button>
                    <button className="btn btn-continue" onClick={submitModal}>Continuar <FaArrowCircleRight /> </button>
                </div>
            </ReactModal>
            <div id="editor-area">
                <div id="editor-useful-area">
                    <canvas id="canvas" />
                </div>
            </div>
            <div id="editor-bar">
                <div className="editor-bar-tools">
                    <h3>Controle</h3>
                    <button onClick={handleUndo}><TiArrowBack className="editor-icon" /></button>
                    <button onClick={handleRedo}><TiArrowForward className="editor-icon" /></button>
                </div>
                <div className="editor-bar-tools">
                    <h3>Adicionar</h3>
                    <button><BsFillImageFill onClick={() => setIsOpen(true)} className="editor-icon" /></button>
                    <button onClick={addText}><ImTextColor className="editor-icon" /></button>
                </div>
                <div className="editor-bar-tools">
                    <h3>Editar</h3>
                    <button onClick={handleClone}><FaRegCopy className="editor-icon" /></button>
                    <button onClick={handleRemove}><BiTrash className="editor-icon" /></button>
                </div>
                <div className="editor-bar-tools">
                    <h3>Ordenar</h3>
                    <button>
                        <FiCopy className="editor-icon icon-order-up" onClick={handleOrderUp} />
                    </button>
                    <button>
                        <FiCopy className="editor-icon icon-order-down" onClick={handleOrderDown} />
                    </button>
                </div>
                <div className="editor-bar-tools">
                    <h3>Posição</h3>
                    <button>
                        <CgAlignBottom
                            className="editor-icon"
                            onClick={() => handleAlign('alignBottom')}
                        />
                    </button>
                    <button>
                        <CgAlignMiddle
                            className="editor-icon"
                            onClick={() => handleAlign('alignMiddle')}
                        />
                    </button>
                    <button>
                        <CgAlignTop
                            className="editor-icon"
                            onClick={() => handleAlign('alignTop')}
                        />
                    </button>
                    <br />
                    <button>
                        <CgAlignLeft
                            className="editor-icon"
                            onClick={() => handleAlign('alignLeft')}
                        />
                    </button>
                    <button>
                        <CgAlignCenter
                            className="editor-icon"
                            onClick={() => handleAlign('alignCenter')}
                        />
                    </button>
                    <button>
                        <CgAlignRight
                            className="editor-icon"
                            onClick={() => handleAlign('alignRight')}
                        />
                    </button>
                </div>
                <div className="editor-bar-tools">
                    <h3>Rotação</h3>
                    <input className="input-editor" value={rotate} onChange={handleRotation} />
                </div>
                <div className={`editor-bar-tools textToolbar ${visibleTextToolbar}`}>
                    <h3>Fonte</h3>
                    <select className="input-editor" value={fontFamily} onChange={handleFontFamily}>
                        <option value="Arial">Arial</option>
                        <option value="Comic Sans MS">Comic Sans MS</option>
                        <option value="Courier New">Courier New</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Trebuchet MS">Trebuchet MS</option>
                        <option value="Verdana">Verdana</option>
                        <option value="Times New Roman">Times New Roman</option>
                    </select>
                    <br />
                    <button>
                        <BiBold className="editor-icon icon-bold" onClick={handleBold} />
                    </button>
                    <button>
                        <CgFormatItalic className="editor-icon icon-italic" onClick={handleItalic} />
                    </button>
                    <button>
                        <input type="color" className="input-color" onChange={handleColor} />
                    </button>
                    <br />
                    <input className="input-editor input-font" value={fontSize} onChange={handleFontSize} /> px
                </div>
            </div>
        </div>
    );
}

export default Editor;