import React, { Component } from 'react';

import './styles.css';

class ProgressBar extends Component {
    // constructor(props) {
    //     super(props);
    // }
    
    render(){
        const percent = position => {
            let percent = [];
            percent[0] = '33%';
            percent[1] = '66%';
            percent[2] = '100%';

            return percent[position];
        }

        return (
            <div id="progressbar">
                <ul>
                    <li className={this.props.position === 0 ? 'active' : ''}>1. Características </li>
                    <li className={this.props.position === 1 ? 'active' : ''}>2. Personalizar</li>
                    <li className={this.props.position === 2 ? 'active' : ''}>3. Finalizar</li>
                </ul>
                <div id="progressbar-bar">
                    <div id="progressbar-complete" style={{width: percent(this.props.position)}}></div>
                </div>
            </div>
        );
    }
    
}

export default ProgressBar;