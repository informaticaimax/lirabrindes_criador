import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { FiSave } from 'react-icons/fi';



import { storageGetCreation, storageSetHistoric, storageGetHistoric, storageRemoveCreation, storageRemoveItem } from '../../services/storage';

import './styles.css';

const Header = (props) => {

    const [idProduct, setIdProduct] = useState(null);
    useEffect(() => {
        let url = window.location.href.split('/');
        let id = url[url.length - 1];
        setIdProduct(id);

        const { pathname } = props.location;

    }, []);

    const handleClose = () => {
        const creation = storageGetCreation(parseInt(idProduct));
        storageRemoveCreation(creation);
        storageRemoveItem('previewJSON');
        window.parent.postMessage({
            'func': 'hideCreator',
        }, "*");
    }
    const handleCloseAndSave = () => {
        let historic = storageGetHistoric(parseInt(idProduct));
        let previewJSON = localStorage.getItem('previewJSON');

        const creation = storageGetCreation(parseInt(idProduct));
        historic = creation;
        if (previewJSON) {
            historic.canvas = previewJSON;
        }
        storageSetHistoric(historic);
        storageRemoveCreation(creation);
        storageRemoveItem('previewJSON');
        window.parent.postMessage({
            'func': 'hideCreator',
        }, "*");
    }

    return (
        <header>
            <div id="menu-actions">
                <ul>
                    <li><button onClick={handleCloseAndSave}><FiSave className="ic-save" /> Fechar e salvar</button></li>
                    <li><button onClick={handleClose}>Fechar edição sem salvar</button></li>
                </ul>
            </div>
        </header>
    );

}

export default withRouter(Header);