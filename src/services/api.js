import axios from 'axios';

const production = true;
const base = production ? 'https://www.lirabrindes.com' : 'http://127.0.0.1:8002';

const api = axios.create({
    baseURL: `${base}/api`
});


export default api;