const app = '@lirabrindes-creator';

export const storageSetItem = (key, value) => {
    localStorage.setItem(`${app}/${key}`, value);
}

export const storageGetItem = (key) => {
    return localStorage.getItem(`${app}/${key}`);
}

export const storageRemoveItem = (key) => {
    localStorage.removeItem(`${app}/${key}`);
}

export const storageSetCreation = (creation) => {

    let creations = storageGetItem('creations');
    creations = creations ? JSON.parse(creations) : [];
    let changed = false;

    creations.forEach((c, index) => {
        if (c.id === creation.id) {
            creations[index] = creation;
            changed = true;
        }
    });

    if (!changed) creations.push(creation);

    storageSetItem('creations', JSON.stringify(creations));
}
export const storageRemoveCreation = (creation) => {

    let creations = storageGetItem('creations');
    creations = creations ? JSON.parse(creations) : [];

    creations.splice(creation);
    storageSetItem('creations', JSON.stringify(creations));
}
export const storageRemoveHistoric = (historic) => {

    let historics = storageGetItem('historics');
    historics = historics ? JSON.parse(historics) : [];

    historics.splice(historic);
    storageSetItem('historics', JSON.stringify(historics));
}
export const storageSetHistoric = (historic) => {

    let historics = storageGetItem('historics');
    historics = historics ? JSON.parse(historics) : [];
    let changed = false;

    historics.forEach((c, index) => {
        if (c.id === historic.id) {
            historics[index] = historic;
            changed = true;
        }
    });

    if (!changed) historics.push(historic);

    storageSetItem('historics', JSON.stringify(historics));
}
export const storageGetHistoric = (id) => {
    let historics = storageGetItem('historics');
    if (!historics) return null;

    historics = JSON.parse(historics);

    let historic = historics.find(c => c.id === id);

    return historic;
}
export const storageGetCreation = (id) => {
    let creations = storageGetItem('creations');
    if (!creations) return null;

    creations = JSON.parse(creations);

    let creation = creations.find(c => c.id === id);

    return creation;
}